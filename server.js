require('dotenv').config()

var fs = require('fs');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const uuid = require('uuid/v1');

const http = require('http');
var url = require('url');

const config = require('./config/config.js');
const isDev = process.env.NODE_ENV !== 'production';

const AWS = require('aws-sdk');

app.use( bodyParser.urlencoded({ extended: true }) );
app.use( bodyParser.json() );

// Obtener todas las imágenes
app.get('/images', function( req, res ){

    if (isDev) {
        AWS.config.update(config.aws_local_config);
    } else {
        AWS.config.update(config.aws_remote_config);
    }

    // Buscar imagen en BD
    const docClient = new AWS.DynamoDB.DocumentClient();
    const params = {
        TableName: config.aws_table_name
    };
    docClient.scan(params, function(err, data) {
        if (err) {
            res.send({
                success: false,
                message: 'Error: Server error'
            });
        } else {
            const { Items } = data;
            res.send({
                success: true,
                message: 'Imágenes',
                fruits: Items
            });
        }
    });

});

// Add image
app.post('/image', (req, res, next) => {

    if (isDev) {
        AWS.config.update(config.aws_local_config);
    } else {
        AWS.config.update(config.aws_remote_config);
    }

    const { name, url } = req.body;

    // Not actually unique and can create problems.
    const imageId = uuid();

    console.log(name);

    const docClient = new AWS.DynamoDB.DocumentClient();
    const params = {
        TableName: config.aws_table_name,
        Item: {
            imageId: imageId,
            imageName: name,
            imageUrl: url
        }
    };

    docClient.put(params, function(err, data) {
        if (err) {
            console.log(err);
            res.send({
                success: false,
                message: 'Error: Server error'
            });
        } else {
            console.log('data', data);
            const { Items } = data;
            res.send({
                success: true,
                message: 'Added image',
                fruitId: imageId
            });
        }
    });

});


// Get a single image by id
app.get('/image', (req, res, next) => {

    if (isDev) {
        AWS.config.update(config.aws_local_config);
    } else {
        AWS.config.update(config.aws_remote_config);
    }

    let imageId = req.query.id;

    console.log( imageId );

    const docClient = new AWS.DynamoDB.DocumentClient();

    const params = {
        TableName: config.aws_table_name,
        KeyConditionExpression: 'imageId = :i',
        ExpressionAttributeValues: {
            ':i': imageId
        }
    };

    console.log( params );


    // #Cambiar por get
    docClient.query(params, function(err, data) {
        if (err) {
            res.send({
                success: false,
                message: 'Error: Server error'
            });
        } else {
            console.log('data', data);
            const { Items } = data;
            res.send({
                success: true,
                message: 'Loaded images',
                fruits: Items
            });
        }
    });

});

app.get('/image_upload', function( req, res, next){

    if (isDev) {
        AWS.config.update(config.aws_local_config);
    } else {
        AWS.config.update(config.aws_remote_config);
    }

    let imageId = req.query.id;

    var docClient = new AWS.DynamoDB.DocumentClient()

    var table = "imagesTable";

    var params = {
        TableName: table,
        Key:{
            'imageId': imageId
        }
    };

    docClient.get(params, function(err, data) {

        const https = require('https');

        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("GetItem succeeded:", JSON.stringify(data.Item, null, 2));

            // let imageUrl = data.Item.imageUrl;

            let imageUrl = 'https://www.walmart.com.mx/images/product-images/img_large/09911050012293l.jpg';

            console.log('request...');

            var DOWNLOAD_DIR = './downloads/';

            var file_name = url.parse(imageUrl).pathname.split('/').pop();
            var file = fs.createWriteStream(DOWNLOAD_DIR + file_name);

            let filepath = file.path;

            console.log( file_name );

            let options = {
                host: url.parse(imageUrl).host,
                port: 80,
                path: url.parse(imageUrl).pathname
            };


            https.get( imageUrl, function( res ){

                res.on('data', function(chunk) {
                    // file.write(chunk);
                    // console.log(chunk);
                    console.log('ahjsdsajdhas');
                }).on('end', function() {
                    // file.end();
                    console.log(file_name + ' downloaded to ' + DOWNLOAD_DIR);
                }).on('error', function(err){

                });
            });



            // if (fs.existsSync(filepath)) {
            //     console.log(`existe`);
            //     let fileBinaryString = fs.readFileSync(filepath, null);
            //
            //     var fileStream = fs.createReadStream(filepath);
            //
            //     const BUCKET_NAME = 'walmart-dynamic-images';
            //     const IAM_USER_KEY = 'AKIAVL76YZVO5WT7ZSFG';
            //     const IAM_USER_SECRET = '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';
            //     const BUCKET_REGION = 'us-east-1';
            //
            //     let s3bucket = new AWS.S3({
            //         accessKeyId: IAM_USER_KEY,
            //         secretAccessKey: IAM_USER_SECRET,
            //         Bucket: BUCKET_NAME,
            //         region: BUCKET_REGION,
            //         apiVersion: '2006-03-01'
            //     });
            //
            //     let params = {
            //         Body: fileStream,
            //         Bucket: BUCKET_NAME,
            //         Key: file_name
            //     };
            //
            //     s3bucket.putObject(params, (e, d) => {
            //         if (e) {
            //             console.log(e);
            //         }
            //
            //         console.log('aki');
            //
            //     });
            //
            // }

        }
    });

    res.send({
        code:0
    });
});

app.get('/image_download', function( req, res, next ){

    const https = require('https');
    let imageUrl = 'https://www.walmart.com.mx/images/product-images/img_large/09911050012293l.jpg';

    console.log('request...');

    var DOWNLOAD_DIR = './downloads/';

    var file_name = url.parse(imageUrl).pathname.split('/').pop();
    var file = fs.createWriteStream(DOWNLOAD_DIR + file_name);

    let filepath = file.path;

    console.log( file_name );

    let options = {
        host: url.parse(imageUrl).host,
        // port: 80,
        path: url.parse(imageUrl).pathname
    };

    https.get( options, function( res ){
        res.on('data', function(chunk) {
            file.write(chunk);
            // console.log('skdjkasdhks');
        }).on('end', function() {
            console.log('enddddd');
            file.end();
            console.log(file_name + ' downloaded to ' + DOWNLOAD_DIR);
        });
    });

    res.send({
        code: 2
    });
});


app.get('/image_to_s3', function( req, res, next ){

    const im = require('imagemagick');

    let imageUrl = 'https://www.walmart.com.mx/images/product-images/img_large/09911050012293l.jpg';

    let DOWNLOAD_DIR = './downloads/';
    let DESTINATION_DIR = './destination/';

    var file_name = url.parse(imageUrl).pathname.split('/').pop();
    var file = fs.createWriteStream(DOWNLOAD_DIR + file_name);

    let filepath = file.path;

    console.log(filepath);

    if (fs.existsSync(filepath)){

        console.log('leer acáaaaa');

        im.readMetadata('09911050012293l.jpg', function(err, metadata){
          if (err) throw err;
          console.log('Shot at '+metadata);
        })

    }



    // if (fs.existsSync(filepath)) {
    //     console.log(`existe`);
    //     let fileBinaryString = fs.readFileSync(filepath);
    //
    //     fs.readFile( filepath, function( err, data ){
    //         console.log(data);
    //     });

    // var fileStream = fs.createReadStream(filepath);

    // const BUCKET_NAME = 'walmart-dynamic-images';
    // const IAM_USER_KEY = 'AKIAVL76YZVO5WT7ZSFG';
    // const IAM_USER_SECRET = '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';
    // const BUCKET_REGION = 'us-east-1';
    //
    // let s3bucket = new AWS.S3({
    //     accessKeyId: IAM_USER_KEY,
    //     secretAccessKey: IAM_USER_SECRET,
    //     Bucket: BUCKET_NAME,
    //     region: BUCKET_REGION
    // });
    //
    // let params = {
    //     Body: fileStream,
    //     Bucket: BUCKET_NAME,
    //     Key: file_name
    // };
    //
    // s3bucket.putObject(params, (e, d) => {
    //     if (e) {
    //         console.log(e);
    //     }
    //
    //     console.log('aki');
    //
    // });

    // }



    res.send({
        code: 1
    });
});


app.listen(3000);
